#!/bin/sh

eval $( perl -Mlocal::lib )

cron -f &
nginx
/start_mb_renderer.pl
start_server --port=55901 -- plackup -I lib -s Starlet -E deployment --nproc 50 --backlog 2048 --max-workers 150 --pid fcgi.pid
